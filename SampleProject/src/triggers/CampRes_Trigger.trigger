trigger CampRes_Trigger on Campsite__c (before insert) {
    //ideally all this code has to move to 
    //say CampResHelper , updateCRs(map<Id,Campsite__c> triggeroldmapCR , list<Campsite__c> triggernewlistCR)
    //so all this functionality will be processed as :
//CampResHelper res = new CampResHelper();
//res.updateCRs(trigger.OldMap,trigger.new);
    
//trigger.new contains all Campsite__c records that got updated
    //getting all child records under the 
    map<Id,list<Campsite_Reservation__c>> childrenReservationMap = new map<Id,list<Campsite_Reservation__c>>();
    for(Campsite_Reservation__c each: [select Id,Campsite__c from Campsite_Reservation__c where Campsite__c in: Trigger.New and Active__c = true]){
        if(childrenReservationMap.containsKey(each.Campsite__c)){
        	childrenReservationMap.get(each.Campsite__c).add(each);
        }
        else{
               //list<Campsite_Reservation__c> newTempList = new list<Campsite_Reservation__c>();
               //newTempList.add(each);
               //childrenReservationMap.put(each.Campsite__c,newTempList);
               childrenReservationMap.put(each.Campsite__c,new list<Campsite_Reservation__c>{each});
        }
    }
    list<Campsite_Reservation__c> listCRToUpdate = new list<Campsite_Reservation__c>();
    for(Campsite__c eachCamp : Trigger.new){
        //filter ones whose status got changed from active to inactive
        //trigger.oldMap holds mapping of Campsite__c Ids and their old records
        if(trigger.oldMap.get(eachCamp.Id).Status__c != false && eachCamp.Status__c == false){
			//fetch all children Campsite Reservation children
            for(Campsite_Reservation__c eachChild :childrenReservationMap.get(eachCamp.Id)){
                eachChild.Active__c = false;
                listCRToUpdate.add(eachChild);
            }
        }
    }
           update listCRToUpdate;
}