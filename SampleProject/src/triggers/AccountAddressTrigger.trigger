trigger AccountAddressTrigger on Account (before insert, before update) {
    for(Account a : Trigger.New) {
        if(a.match_billing_address__c == true){
           a.ShippingPostalCode = a.BillingPostalCode;
        }
    }
}