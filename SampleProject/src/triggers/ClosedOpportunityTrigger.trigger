trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {

	List<Opportunity> opps = [SELECT Id, StageName FROM Opportunity WHERE StageName = 'Closed Won']; 
 	List<Task> tasks = new List<Task>();
    
    for(Opportunity opp : opps) {
            Task newTask = new Task(subject = 'Follow Up Test Task');
            newTask.WhatId = opp.Id;
            tasks.add( newTask ); 
    }    
    
    insert( tasks); 
    
}