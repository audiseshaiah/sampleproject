public class AESEncryptionDecryptionUtil {

    public string username { set; get; }
    public string password { set; get; }
    public String text { set; get; }
    public String encryptedText { get; set; }
    public String decryptedText { get; set; }
    
    public Integer userNameStartingCharacter;
    public Integer userNameEndingCharacter;
    public Integer passwordStartingCharacter;
    public Integer passwordEndingCharacter;
    public String encryptionType;
    public String documentName;
    public String key;
    
    
    public void decrypt() {
    
        decryptedText = Crypto.decryptWithManagedIV('AES256', Blob.valueOf(key), EncodingUtil.base64Decode(encryptedText)).toString();
        
        System.debug('decryptedText '+decryptedText);
        
        /*String username = userName;
        String password = password;
        String text = text;
        System.debug('UserName '+userName);
        System.debug('Password '+password);
        System.debug('Text '+text);*/
        
    }

    public void encrypt() {
        fetchEncryptionSettings();
       // text = readTextFromFile('Dump');
        encryptedText = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', Blob.valueOf(key), readTextFromFile('Dump')));
        
        System.debug('encryptedText --->'+encryptedText);
        
        //decryptedText = Crypto.decryptWithManagedIV('AES256', Blob.valueOf(key), EncodingUtil.base64Decode(encryptedText)).toString();
        
        //System.debug('decryptedText --->'+decryptedText);
        /*String username = userName;
        String password = password;
        String text = text;
        System.debug('Encrypt - UserName '+userName);
        System.debug('Encrypt - Password '+password);
        System.debug('Encrypt - Text '+text); */
        
    }


    public void fetchEncryptionSettings() {
        
        ENCRYPTION__c encryptionSettings = ENCRYPTION__c.getInstance(Userinfo.getUserId());
        System.debug(encryptionSettings);
        userNameStartingCharacter = Integer.valueOf(String.valueOf(encryptionSettings.get('UserNameStartingCharacter__c')).trim());
        userNameEndingCharacter = Integer.valueOf(String.valueOf(encryptionSettings.get('UserNameEndingCharacter__c')).trim());
        passwordStartingCharacter = Integer.valueOf(String.valueOf(encryptionSettings.get('PasswordStartingCharacter__c')).trim());
        passwordEndingCharacter = Integer.valueOf(String.valueOf(encryptionSettings.get('PasswordEndingCharacter__c')).trim());
        encryptionType = String.valueOf(encryptionSettings.get('EncryptionType__c')).trim();
        documentName = String.valueOf(encryptionSettings.get('DocumentName__c')).trim();
        
        System.debug('userNameStartingCharacter '+userNameStartingCharacter + '\n' +
                     'userNameEndingCharacter '+userNameEndingCharacter + '\n' +
                     'passwordStartingCharacter '+passwordStartingCharacter + '\n' +
                     'passwordEndingCharacter '+passwordEndingCharacter + '\n' +
                     'encryptionType '+encryptionType + '\n' + 
                     'documentName '+documentName );
                     
        key = generatePrivateKey();
        System.debug('Key ---->'+key);
    }

    public String generatePrivateKey(){
        //TODO: Implement private key generation logic here
        String tmpKey = username.subString(userNameStartingCharacter, userNameEndingCharacter) + encryptionType + password.subString(passwordStartingCharacter,  passwordEndingCharacter);
        return convertToNumberString(tmpKey).substring(0,32);
    }
    
    public String convertToNumberString(String key){
        String numberString = '';
        Integer[] tmp = key.getChars();
        System.debug('characters ---> '+tmp);
        For(Integer i : tmp) {
            numberString = numberString + i;
        }
        
        return numberString;
    }

    public Blob readTextFromFile(String fileName)
    {
        Blob b = null;
        List<Document> d = [Select body, bodyLength, ContentType, Url from Document where Name =:fileName];
        if(d.size()>0)
        {
             b = d[0].body;
             //System.debug('Content -->\n'+b.toString());
        }
     return b;
    }

    public static String encrypt(String userName, String password, String text) {
        String encryptedText = '';
        
        return encryptedText;
    }


    public static String decrypt(String userName, String password, String text) {
        String decryptedText = '';
        
        return decryptedText;
    }

}