public class AccountHandler {
	//Test Comment - Audi - 11/30/2016
    public static Account insertNewAccount(String accName){
        if(String.isBlank(accName) || String.isEmpty(accName)){
           return null;
        }else {
        Account acc = new Account(Name=accName);
        Database.SaveResult sr = Database.insert(acc, false);
        
        if(sr.isSuccess()){
            System.debug('Successfully inserted..');
            return acc;
        }else {
            System.debug('Error while inserting..');
            return null;
            }
        }
        
    }    
}