public class StringArrayTest {

    public static List<String> generateStringArray(integer size){
        System.debug('Method start');
        List<String> lstStrings = new List<String>();
        for(integer i=0;i<size; i++){
            lstStrings.add('Test '+i);
        }
        System.debug('Method end');
        return lstStrings;
    }
}