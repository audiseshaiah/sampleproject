public with sharing class ClassAccountList2Controller {

    public Account acc {set; get;}
    
    public ClassAccountList2Controller(){
        acc = new Account();
    }

 public pagereference Save(){
        acc.rating= 'Warm';
        insert acc;
        pagereference page=new pagereference('/apex/ClassAccountList3?id='+acc.id);
        return page;
    }
    
    public void clear(){
        acc = new Account();
    }
    
    public pagereference cancel(){
        pagereference page=new pagereference('/001/o');
        return page;
    }

}