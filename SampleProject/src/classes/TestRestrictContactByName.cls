@isTest
public class TestRestrictContactByName {

    @isTest
     static void VerifyRestrictContactByNameTrigger(){
    
        Contact contact1 = new Contact(firstname ='Audi', lastname='INVALIDNAME');
        //insert contact1;
        
        Test.startTest();
        Database.saveresult result = Database.insert(contact1, false);
        Test.stopTest();
        SYSTEM.debug('VerifyRestrictContactByNameTrigger - result.isSuccess() '+result.isSuccess());        
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML', result.getErrors()[0].getMessage());

    
    }
    
    
     @isTest(seealldata=true)
     static void VerifyRestrictContactByNameTrigger3(){
    
        Contact contact1 = [SELECT FIRSTNAME, LASTNAME FROM CONTACT where firstname='Audi' LIMIT 1];
        CONTACT1.LASTNAME = 'INVALIDNAME';
        //insert contact1;
        
        Test.startTest();
        Database.saveresult result = Database.UPDATE(contact1, false);
        Test.stopTest();
        SYSTEM.debug('VerifyRestrictContactByNameTrigger - result.isSuccess() '+result.isSuccess());        
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML', result.getErrors()[0].getMessage());

    
    }

    @isTest(seealldata=true)
    static void VerifyRestrictContactByNameTrigger4(){
    
        Contact contact1 = [SELECT FIRSTNAME, LASTNAME FROM CONTACT LIMIT 1];
        contact1.lastname = 'test8762';
        //insert contact1;
        
        Test.startTest();
        Database.saveresult result = Database.update(contact1, false);
        Test.stopTest();
        system.debug('result.isSuccess() '+result.isSuccess());
        system.debug('result.getErrors().size() '+result.getErrors().size());
        System.assert(result.isSuccess());
        System.assert(result.getErrors().size() == 0);
        
    
    }
    
    @isTest
    static void VerifyRestrictContactByNameTrigger2(){
    
        Contact contact1 = new Contact(firstname ='Audi', lastname='Maddali123487');
        //insert contact1;
        
        Test.startTest();
        Database.saveresult result = Database.insert(contact1, false);
        Test.stopTest();
        system.debug('result.isSuccess() '+result.isSuccess());
        system.debug('result.getErrors().size() '+result.getErrors().size());
        System.assert(result.isSuccess());
        System.assert(result.getErrors().size() == 0);
        
    
    }
}