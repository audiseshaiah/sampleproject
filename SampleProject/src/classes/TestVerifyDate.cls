@isTest
public class TestVerifyDate {

     /*static void compareDate(){
      DateTest dtTest = new DateTest();
          Date resultDate = dtTest.compareDate(date.newinstance(2016, 1, 25),date.newinstance(2015, 12, 10));
      System.assertEquals(date.newinstance(2015, 12, 31), date.newinstance(2015, 12, 31));    
    } */
    @isTest
    static void CheckDatesTest(){ 
        //VerifyDate verify = new VerifyDate();
         Date resultDate = VerifyDate.CheckDates(date.newinstance(2015, 12, 31), date.newinstance(2015, 12, 30));
         System.assertEquals(date.newinstance(2015, 12, 31), resultDate );
    }
    
    @isTest
    static void CheckDatesTest2(){
        //VerifyDate verify = new VerifyDate();
         Date resultDate = VerifyDate.CheckDates(date.newinstance(2015, 12, 1), date.newinstance(2015, 12, 20));
         System.assertEquals(date.newinstance(2015, 12, 20), resultDate );
    }
    
    @isTest
    static void DateWithin30DaysTest2(){
        //VerifyDate verify = new VerifyDate();
         Date resultDate = VerifyDate.CheckDates(date.newinstance(2015, 12, 01), date.newinstance(2016, 12,20));
         System.assertEquals(date.newinstance(2015, 12, 31), resultDate );
    }
}