public with sharing class ClassAccountList3Controller {

public Account acc {set; get;}
    
    public ClassAccountList3Controller(){
        acc = new Account();
    }

 public void catchParam(){
    String param = ApexPages.currentpage().getparameters().get('id');
    acc = [select name, accountnumber from account where id = :param limit 1];
    
    }
    
    public void clear(){
        acc = new Account();
    }
    
    public pagereference cancel(){
        pagereference page=new pagereference('/001/o');
        return page;
    }

}