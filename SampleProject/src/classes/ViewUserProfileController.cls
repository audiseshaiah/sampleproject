public with sharing class ViewUserProfileController {

	public User currentUser { get; set; }

	public ViewUserProfileController(){
		System.debug('calling default constructor');
	}

	public ViewUserProfileController(ApexPages.StandardController controller){
		System.debug('overridden constructor');
	}

    public PageReference userDetails() {
    	System.debug('userDetails method - start');
    	currentUser = new User();
    	currentUser = [SELECT City,ContactId,Email,FirstName,Gender__c,Id,LastName,MobilePhone,Name,Phone,PostalCode,Username FROM User where Id= :UserInfo.getuserId() LIMIT 1];
        System.debug(currentUser);
        System.debug('userDetails method - end');
        return null; 
    }

    public PageReference EditUserProfile(){
    	System.debug('EditUserProfile - Method');
    	PageReference reference = new PageReference('/apex/EditUserProfile');
    	return reference;
    }

	public PageReference Cancel(){
    	System.debug('Cancel - Method');
    	return null;
    }
}