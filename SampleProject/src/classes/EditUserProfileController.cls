public with sharing class EditUserProfileController {
	
	public User currentUser { get; set; } 
	public Contact currentContact { get; set; }

	public EditUserProfileController() {
		System.debug('EditUserProfileController - Default Constructor');		
	}

	public EditUserProfileController(ApexPages.StandardController controller){
		System.debug('EditUserProfileController - Overriden Constructor');	
	}

	public PageReference editUserDetails() {
		currentUser = new User();
		currentContact = new Contact();
		currentUser = [SELECT City,ContactId,Email,FirstName,Gender__c,Id,LastName,MobilePhone,Name,Phone,PostalCode,Username FROM User where Id= :UserInfo.getuserId() LIMIT 1];
		currentContact = [SELECT FirstName,Gender__c,LastName,MailingCity,MailingPostalCode,MobilePhone,Name,Phone FROM Contact where Id=:currentUser.ContactId];
        System.debug(currentUser);
        return null;
	}

	public PageReference SaveUserProfile(){
		PageReference reference = new PageReference('/apex/ViewUserProfile');
		System.debug(currentUser);
		//currentContact.Name = currentUser.Name;
		currentContact.FirstName = currentUser.FirstName;
		currentContact.LastName = currentUser.LastName;
		currentContact.Gender__c = currentUser.Gender__c;
		currentContact.MobilePhone = currentUser.MobilePhone;
		currentContact.Phone = currentUser.Phone;
		currentContact.MailingCity = currentUser.City;
		currentContact.MailingPostalCode = currentUser.PostalCode;

		update currentUser;
		update currentContact;

		return reference; 
	}
	public PageReference cancel(){

		return null;
	}
}