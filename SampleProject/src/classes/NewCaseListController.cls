public with sharing class NewCaseListController {

    

public Case cs {set; get;}
public List<Case> cases {set; get;}

public NewCaseListController(){
    cs = new Case();
    cases = new List<Case>();

}
public List<Case> getNewCases(){
    cases = [select Id, CaseNumber, subject from case where status = 'New' limit 10];
    return cases;
}
/*public List<Case> getNewCases(){
    
     cases = [select Id, CaseNumber from case where Status='New'];
    
    return cases;

}*/

}