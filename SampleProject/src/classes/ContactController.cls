public with sharing class ContactController {
	
	public String accId {get; set;}
	public List<Contact> contacts {get; set;}
	public ContactController() {
		//accId = ApexPages.CurrentPage().getParameters().get('accId');
		accId='0012800000Cy9uD';
	}

	public PageReference loadContacts(){
		contacts = new List<Contact>();
		contacts = [select Contact.Name, Contact.Id from Contact where AccountId=:accId];
		System.debug('Contacts --> '+contacts);
		return null;
	}
}