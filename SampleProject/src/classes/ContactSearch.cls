public class ContactSearch {

    public static List<Contact> searchForContacts(String lastName, String postalCode){
        
        List<Contact> contactList = [select id, name from contact where lastname=:lastName and MailingPostalCode=:postalCode];
        
        return contactList;
    }
}