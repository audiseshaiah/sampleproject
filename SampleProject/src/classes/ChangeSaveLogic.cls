public class ChangeSaveLogic {

    public Account acc {set; get;}
    
    public ChangeSaveLogic(ApexPages.StandardController controller) {
        acc = new Account();

    }
    
    public pagereference Save(){
        acc.rating= 'Warm';
        insert acc;
        pagereference page=new pagereference('/'+acc.id);
        return page;
    }
    
    public pagereference cancel(){
        pagereference page=new pagereference('/001/o');
        return page;
    }

}