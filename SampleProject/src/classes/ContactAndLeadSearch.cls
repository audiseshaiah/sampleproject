public class ContactAndLeadSearch {

    public static List<List<sObject>> searchContactsAndLeads(String nameStr){
        List<List<sObject>> results = [FIND :nameStr IN NAME FIELDS RETURNING Contact, Lead];
        
        return results;
        
    }
}