public with sharing class EditProfileController {
	
	public EditProfileController(){
		System.debug('Default Controller '+userinfo.getuserId());
	}
	public User currentUser {get; set;}
	
    public EditProfileController(ApexPages.StandardController controller) {
		User usr = (User) controller.getRecord();
		System.debug('User from Controller '+usr);
		
		
    }
    
    public Pagereference fetchContact(){
    	
    	System.debug('fetchContact '+userinfo.getuserId());
    	currentUser = new User();
		currentUser=[Select Id, Name, FirstName, LastName, Email, Username, contactId, Address, Gender__c  from User where Id=:userinfo.getuserId()];
		System.debug('Current User '+currentUser);
    	return null;
    }

	public Pagereference EditUserProfile(){
		System.debug('Edit method');
		Pagereference reference = new Pagereference('/apex/EditUserProfile?uid='+userInfo.getUserId());
		return reference;
	}
	public Pagereference Save(){
		System.debug('Save method');
		return null;
	}
	public Pagereference Cancel(){
		System.debug('Cancel method');
		return null;
	}
}