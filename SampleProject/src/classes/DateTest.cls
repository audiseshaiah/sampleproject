public with sharing class DateTest {

    //Compare two dates 
    public Date compareDate(Date dt1, Date dt2) {
        
        Date resultDate;
        System.debug('Date 1 --> '+dt1);
        System.debug('Date 2 --> '+dt2);
        System.debug(dt1.daysBetween(dt2)); 
        //If date1 is leass than date2 and the difference is less than 30 days return Date1
        if(dt1<dt2 && ((dt2.daysBetween(dt1)) < 30) ) {
            
            resultDate = dt1;
            
        } else 
        // date1 is greater than date2 and difference is greater than 30 days return last date of the Date 2 month
        if(dt1>dt2 && ((dt2.daysBetween(dt1)) > 30)) {
            
            //Get no. of days in the month for date 2
            Integer lastDate = Date.daysInMonth(dt2.year(), dt2.month());
            System.debug('lastDate --> '+lastDate);
            //Construct the late day of the month
            resultDate = Date.newInstance(dt2.year(), dt2.month(), lastDate);
            
        }    
    return resultDate ;
    }
    
}