public class RandomContactFactory {

    public static List<Contact> generateRandomContacts(Integer noOfContacts, String lName){
        
        List<Contact> contacts = new List<Contact>();
        Contact contact = null;
        for(Integer i=1; i<=noOfContacts; i++ ){
            
            contact = new Contact(firstname = 'Test '+i, lastname=lName);
            contacts.add(contact);
        }
        return contacts;
    }
    
}